# timer

An efficient event scheduler that uses a set of indices to represent the base-2 logarithm of the time remaining in the event

Index 0 represents 2^0 remaining milliseconds, index 1 represents 2^1, and so on up to 2^32

When an event is created, it is placed in the largest index that the remaining time can fit into without going over

For example, an event 1000 milliseconds in the future would be put in index 9

After 512 milliseconds, the event is re-evaluated. Since it has 488ms remaining, it is sorted into index 8 to wait for 256ms, and so on until the event is ready. To avoid checking all 32 indices at every dispatch, a set of internal events are used that indicate when to check the next higher index such that every index is checked twice for each full duration.

```
# test 500 events over 5 seconds
make
./timer-test 0 5000 500
```

Usage

```c++
  typedef std::chrono::system_clock Clock;

  Timer<Clock> clock;
  Timer<Clock>::Event ev;

  Timer<Clock>::Point start = clock.now();

  clock.set(&ev, std::chrono::milliseconds(2100));

  Clock::duration s = clock.left();

  bool ready = false;

  while (!ready) {
    std::this_thread::sleep_for(clock.left());
    ready = clock.advance();
  }

  Timer<Clock>::Event *ev2 = clock.next();

  printf("%lx %lx\n", (uintptr_t)&ev, (uintptr_t)ev2);

  Timer<Clock>::Point end = clock.now();

  uint64_t millis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

  printf("%lu\n", millis);
```

For a more comprehensive usage example, please see `test/test.cpp`

