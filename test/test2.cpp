#include <limits.h>
#include <stdio.h>

#include <thread>
#include "timer.hpp"

typedef std::chrono::steady_clock Clock;

int main(int argc, char **argv) {
  Timer<Clock> clock;
  Timer<Clock>::Event ev;

  Timer<Clock>::Point start = clock.now();

  clock.set(&ev, std::chrono::milliseconds(2100));

  Clock::duration s = clock.left();

  bool ready = false;

  while (!ready) {
    std::this_thread::sleep_for(clock.left());
    ready = clock.advance();
  }

  Timer<Clock>::Event *ev2 = clock.next();

  printf("%lx %lx\n", (uintptr_t)&ev, (uintptr_t)ev2);

  Timer<Clock>::Point end = clock.now();

  uint64_t millis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

  printf("%lu\n", millis);

  return 0;
}
