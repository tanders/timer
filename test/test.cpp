#include <limits.h>
#include <stdio.h>

#include <thread>
#include "timer.hpp"

typedef std::chrono::steady_clock Clock;

uint32_t timer_range_min       = 0;
uint32_t timer_range_max       = 0;
uint32_t timer_count           = 0;

uint32_t minute_count          =    0;
uint32_t minute_dispatch_count =    0;
double   minute_dispatch_time  =  0.0;
double   minute_dispatch_min   = -1.0;
double   minute_dispatch_max   =  0.0;

uint32_t total_count           =    0;
uint32_t total_dispatch_count  =    0;
double   total_dispatch_time   =  0.0;
double   total_dispatch_min    = -1.0;
double   total_dispatch_max    =  0.0;


void minute_reset(void)
{
  minute_count          =    0;
  minute_dispatch_count =    0;
  minute_dispatch_time  =  0.0;
  minute_dispatch_min   = -1.0;
  minute_dispatch_max   =  0.0;

  return;
}


void dispatch_minmax(double d)
{
  if (minute_dispatch_min == -1.0)
  {
    minute_dispatch_min = d;
  }

  if (total_dispatch_min == -1.0)
  {
    total_dispatch_min = d;
  }

  if (d < minute_dispatch_min)
  {
    minute_dispatch_min = d;
  }

  if (d < total_dispatch_min)
  {
    total_dispatch_min = d;
  }

  if (d > minute_dispatch_max)
  {
    minute_dispatch_max = d;
  }

  if (d > total_dispatch_max)
  {
    total_dispatch_max = d;
  }

  return;
}

uint32_t timer_rand(void)
{
  double r;
  uint32_t ir;

  r = (double)rand() / (double)RAND_MAX;

  ir = (uint32_t)(r * (double)(timer_range_max - timer_range_min));
  ir += timer_range_min;

  return ir;
}

void timer_fire(void)
{
  minute_count++;
  total_count++;
  timer_count--;
}


void minute_announce(uint32_t minute)
{
  double minute_dispatch_average;
  double total_dispatch_average;

  minute_dispatch_average = minute_dispatch_time / (double)minute_dispatch_count;
  total_dispatch_average = total_dispatch_time / (double)total_dispatch_count;

  printf("minute: %u\n", minute);
  printf("  fired: %u\n", minute_count);
  printf("  dispatches: %u\n", minute_dispatch_count);
  printf("  average dispatch time: %f\n", minute_dispatch_average);
  printf("  min dispatch time: %f\n", minute_dispatch_min);
  printf("  max dispatch time: %f\n", minute_dispatch_max);
  printf("totals:\n");
  printf("  fired: %u\n", total_count);
  printf("  left: %u\n", timer_count);
  printf("  dispatches: %u\n", total_dispatch_count);
  printf("  average dispatch time: %f\n", total_dispatch_average);
  printf("  min dispatch time: %f\n", total_dispatch_min);
  printf("  max dispatch time: %f\n", total_dispatch_max);
  printf("\n");
}


int main(int argc, char **argv)
{
  int i = 0;
  Timer<Clock>::Duration d;
  uint32_t minute_next = 1;

  Timer<Clock>      timer;
  Clock::time_point dispatch_start;
  Clock::time_point dispatch_end;
  Clock::time_point start;
  Clock::time_point end;

  if (argc != 4)
  {
    printf("usage: %s <min> <max> <count>\n", argv[0]);
    exit(1);
  }

  timer_range_min = strtoul(argv[1], NULL, 10);\

  if (timer_range_min == ULONG_MAX && errno == ERANGE)
  {
    printf("invalid min value given\n");
    exit(1);
  }

  timer_range_max = strtoul(argv[2], NULL, 10);

  if (timer_range_max == ULONG_MAX && errno == ERANGE)
  {
    printf("invalid max value given\n");
    exit(1);
  }

  timer_count = strtoul(argv[3], NULL, 10);

  if (timer_count == ULONG_MAX && errno == ERANGE)
  {
    printf("invalid count value given\n");
    exit(1);
  }

  if (timer_range_max < timer_range_min)
  {
    printf("invalid range: max is less than min\n");
    exit(1);
  }

  srand(time(NULL));

  printf("generating timers...");

  start = timer.now();

  for (i = 0; i < timer_count; i++)
  {
    Timer<Clock>::Event *ev = new Timer<Clock>::Event(timer_fire);
    timer.set(ev, std::chrono::milliseconds(timer_rand()));
  }

  end = timer.now();

  printf(" done generating %d timers (%lu msec)\n", i, 
    std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());

  start = timer.now();

  while (timer_count > 0)
  {
    std::this_thread::sleep_for(timer.left());

    dispatch_start = timer.now();

    timer.advance();
    while (timer.ready()) {
      Timer<Clock>::Event *e = timer.next();
      e->fn()();
    }

    dispatch_end = timer.now();

    d = std::chrono::duration_cast<std::chrono::milliseconds>(dispatch_end - dispatch_start);

    minute_dispatch_count++;
    total_dispatch_count++;

    minute_dispatch_time += d.count();
    total_dispatch_time += d.count();

    dispatch_minmax((double)d.count());

    /* determine if a minute has ended, if it has, announce */
    end = timer.now();

    if (end >= start + std::chrono::milliseconds(60000 * minute_next))
    {
      minute_announce(minute_next);
      minute_reset();

      minute_next++;
    }
  }

  minute_announce(minute_next);

  return 0;
}
