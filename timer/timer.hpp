#pragma once

#include <iostream>
#include <chrono>
#include <cstdint>
#include <functional>

template <typename Clock>
class Timer {
public:
  class Event;

  typedef typename Clock::time_point Point;
  typedef std::chrono::milliseconds  Duration;
  typedef std::function<void(void)>  Fn;

private:
  class Index;

  // indices to track (2 ** s_indices) milliseconds
  static const uint32_t s_indices = 32; // 4.3 billion milliseconds

  // used to indicate event completion
  static constexpr uintptr_t s_done = (~(uintptr_t)0);

  Clock m_clock;            // clock instantiation
  Point m_next;             // time of the next event
  Point m_last;             // time of the most recent advance
  Index m_done;             // completed events
  Index m_index[s_indices]; // time points to consider

  static uint32_t ev_calculate(Event *ev, Point n) {
    if (ev->m_at <= n) {
      return 0;
    }

    uint64_t diff = std::chrono::duration_cast<std::chrono::milliseconds>(ev->m_at - n).count();

    if (diff >= ~(uint32_t)0) {
      return s_indices - 1;
    }

    uint32_t r = 0;
    uint32_t shift = 0;

    shift = ((diff & 0xFFFF0000) != 0) << 4; diff >>= shift; r |= shift;
    shift = ((diff & 0xFF00    ) != 0) << 3; diff >>= shift; r |= shift;
    shift = ((diff & 0xF0      ) != 0) << 2; diff >>= shift; r |= shift;
    shift = ((diff & 0xC       ) != 0) << 1; diff >>= shift; r |= shift;
    shift = ((diff & 0x2       ) != 0) << 0; diff >>= shift; r |= shift;

    return r;
  }

  static bool ev_valid(Event *p) {
    return p != nullptr && p != (Event *)s_done;
  }

  class Index {
    Event m_head;

  public:

    Index() {
      m_head.m_prev = &m_head;
      m_head.m_next = &m_head;
    }

    static bool remove(Event *ev) {
      if (ev == nullptr || !ev_valid(ev->m_prev) || !ev_valid(ev->m_next)) {
        return false;
      }

      ev->m_prev->m_next = ev->m_next;
      ev->m_next->m_prev = ev->m_prev;

      return true;
    }

    bool empty(void) {
      return m_head.m_next == &m_head;
    }

    Event *first() {
      Event *ev = m_head.m_next;

      if (ev == &m_head) {
        return nullptr;
      }

      return ev;
    }

    Event *head() {
      return &m_head;
    }

    Event *pop() {
      if (m_head.m_prev == &m_head) {
        return nullptr;
      }

      Event *r = m_head.m_next;

      Index::remove(r);

      return r;
    }

    void push(Event *ev) {
      if (ev == nullptr) {
        return;
      }

      ev->m_next = &m_head;
      ev->m_prev = m_head.m_prev;

      m_head.m_prev->m_next = ev;
      m_head.m_prev = ev;
    }
  };

public:

  class Event {
    friend class Index;
    friend class Timer<Clock>;

    Fn     m_fn;
    Point  m_at;
    Event *m_prev;
    Event *m_next;

  public:

    Event(void) :
      m_fn(nullptr), m_at(Point()), m_prev(nullptr), m_next(nullptr) {}

    Event(Fn fn) : 
      m_fn(fn), m_at(Point()), m_prev(nullptr), m_next(nullptr) {}

    ~Event() {
      if (this->waiting()) {
        Index::remove(this);
      }
    }

    bool operator()(void) {
      if (this->ready()) {
        m_prev = nullptr;
        m_next = nullptr;
        m_fn();
        return true;
      } else {
        return false;
      }
    }

    Point at(void) {
      return m_at;
    }

    bool cancel(void) {
      if (this->fresh()) {
        return false;
      }

      if (this->waiting()) {
        Index::remove(this);
      }

      m_prev = nullptr;
      m_next = nullptr;
      return true;
    }

    Fn fn(void) {
      return m_fn;
    }

    Fn fn(Fn f) {
      Fn t = m_fn;
      m_fn = f;
      return t;
    }

    bool fresh(void) {
      return m_prev == nullptr && m_next == nullptr;
    }

    bool ready(void) {
      return m_prev == (Event *)s_done && m_next == (Event *)s_done;
    }

    bool waiting(void) {
      return ev_valid(m_prev) && ev_valid(m_next);
    }
  };

  Timer() {
    // set up index heads with the time to check the next index
    Point n = m_clock.now();

    for (uint32_t i = 0; i < (s_indices - 1); i++) {
      Event *ev = m_index[i].head();
      ev->m_at = n + std::chrono::milliseconds(1 << i);
    }

    m_next = m_index[0].head()->m_at;
    m_last = n;
  }

  // amount of time until next event
  typename Clock::duration left(void) {
    Point n = m_clock.now();

    if (n > m_next) {
      return typename Clock::duration(0);
    } else {
      return std::chrono::duration_cast<typename Clock::duration>(m_next - n);
    }
  }

  // get an event off the done list
  Event *next(void) {
    Event *ev = m_done.pop();
    ev->m_prev = (Event *)s_done;
    ev->m_next = (Event *)s_done;
    return ev;
  }

  Point now(void) {
    return m_clock.now();
  }

  // return true if there are events in done
  bool ready(void) {
    return !m_done.empty();
  }

  // set an event to go off after duration
  bool set(Event *ev, Duration after) {
    if (ev == nullptr || ev->m_next != nullptr || ev->m_prev != nullptr) {
      return false;
    }

    if (after <= Duration(0)) {
      m_done.push(ev);
    } else {
      this->set(ev, m_clock.now() + after);
    }

    return true;
  }

  // set an event to go off at point
  bool set(Event *ev, Point at) {
    if (ev == nullptr || ev->m_next != nullptr || ev->m_prev != nullptr) {
      return false;
    }

    ev->m_at = at;

    if (at < m_last) {
      m_done.push(ev);
    } else {
      if (at < m_next) {
        m_next = at;
      }

      m_index[0].push(ev);
    }

    return true;
  }

  // scan for ready events, return true if there are done events
  bool advance(void) {
    if (this->ready()) {
      return true;
    }

    uint32_t i;

    Point n = m_clock.now();
    Point m = n + std::chrono::milliseconds(~(uint32_t)0);

    for (i = 0; i < s_indices; i++) {
      Event *ev;

      while ((ev = m_index[i].first()) != nullptr) {
        uint32_t x = ev_calculate(ev, n);

        // if event is not done, update nearest next event 
        if (x != 0 && ev->m_at < m) {
          m = ev->m_at;
        }

        if (x == 0) {
          // done
          m_done.push(m_index[i].pop());
        } else if (x != i) {
          // change indices
          m_index[x].push(m_index[i].pop());
        } else {
          // still waiting
          break;
        }
      }

      if (i < (s_indices - 1)) {
        Event *h = m_index[i].head();

        if (h->m_at <= n) {
          // reset head and consider next index
          h->m_at = n + std::chrono::milliseconds(1 << i);
        } else {
          // still waiting
          break;
        }
      }
    }

    if (m == (n + std::chrono::milliseconds(~(uint32_t)0))) {
      m = m_index[i].head()->m_at;
    }

    m_next = m;

    return this->ready();
  }
};
